import requests
import smtplib
import schedule
import os
import paramiko
import linode_api4
import time

EMAIL_USERNAME = os.environ.get('EMAIL_USERNAME')
EMAIL_PASSWORD = os.environ.get('EMAIL_PASSWORD')
LINODE_PAT = os.environ.get('LINODE_PAT')
linode = linode_api4.LinodeClient(LINODE_PAT)


def send_mail(subject, body):
    with smtplib.SMTP('smtp.gmail.com', 587) as smtp:
        smtp.starttls()
        smtp.ehlo()
        # https://myaccount.google.com/u/0/apppasswords
        smtp.login(EMAIL_USERNAME, EMAIL_PASSWORD)

        message = 'Subject: {}\n\n{}'.format(subject, body)
        smtp.sendmail(EMAIL_USERNAME, EMAIL_USERNAME, message)


# restart docker container for nginx app using ssh and docker start
def restart_docker_container():
    print("restarting container")
    ssh = paramiko.SSHClient()
    ssh.load_system_host_keys()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(hostname='194.195.251.197', username='root', password="Il2lpadl$123")
    stdin, stdout, stderr = ssh.exec_command('docker ps')
    # print(stdin)
    print(stdout.readlines())

    stdin, stdout, stderr = ssh.exec_command('docker start f15cbce23f5a')
    # print(stdin)
    print(stdout.readlines())
    ssh.close()
    send_mail("Docker Container Restarted", "Very pro-active hey?")


def restart_linode_server():
    print("rebooting the server")
    nginx_server = linode.load(linode_api4.Instance, 44943450)
    nginx_server.reboot()
    send_mail("Server Restarting...", "Very pro-active hey?")

    while True:
        nginx_server = linode.load(linode_api4.Instance, 44943450)
        # print(nginx_server.status)
        if nginx_server.status == 'running':
            time.sleep(5)
            send_mail("Server is back up...","Phew")
            restart_docker_container()
            break


def restart_server_and_container(the_exception):
    print("website down!")
    send_mail("SITE NOT ACCESSIBLE", "Fix the issue!")
    # restart linode server
    restart_linode_server()


def monitor_webapp():
    try:
        # check website by sending it a request, then check the response and handle it
        response = requests.get("http://194-195-251-197.ip.linodeusercontent.com:8080/")
        if response.status_code == 200:
            send_mail("Website Up!", "How nice!")
            print("website up!")
        else:
            print("website down!")
            send_mail("Website DOWN!", "Yikes - attempting self heal with restart!")

            # restart docker container for nginx app using ssh and docker start
            restart_docker_container()

    except Exception as ex:
        restart_server_and_container(ex)


schedule.every(1).minutes.do(monitor_webapp)

while True:
    schedule.run_pending()
