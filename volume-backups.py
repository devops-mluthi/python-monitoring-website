import schedule
import boto3

def create_volume_snapshots():
    client = boto3.client('ec2', region_name="ap-southeast-2")
    volumes = client.describe_volumes(
        Filters=[
            {
                'Name': 'tag:Name',
                'Values': ['prod']
            }
        ]
    )['Volumes']
    print(volumes)

    for volume in volumes:
        new_snapshot = client.create_snapshot(
            VolumeId=volume['VolumeId']
        )
        print(new_snapshot)

schedule.every(5).seconds.do(create_volume_snapshots)

while True:
    schedule.run_pending()
