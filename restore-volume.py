import boto3
from operator import itemgetter

# 1: get volumes given an ec2 instance id - assume only one volume
# 2: get snapshots from volume and sort by date desc
# 3: create new volume from snapshot
# 4: attach/replace new volume to the given instance

client = boto3.client('ec2', region_name="ap-southeast-2")

instance_id = "i-00db52be403350e4a"

volumes = client.describe_volumes(
    Filters=[
        {
            'Name': 'attachment.instance-id',
            'Values': [instance_id]
        }
     ]
)['Volumes']

volume_id = volumes[0]['VolumeId']

print(volume_id)

snapshots = client.describe_snapshots(
    OwnerIds=['self'],
    Filters=[
        {
            'Name': 'volume-id',
            'Values': [volume_id]
        }
    ]
)['Snapshots']

# 2: get snapshots from volume and sort by date desc
latest_snapshot = sorted(snapshots, key=itemgetter('StartTime'), reverse=True)[0]
latest_snapshot_id = latest_snapshot['SnapshotId']
print(latest_snapshot)


# 3: create new volume from snapshot
new_volume = client.create_volume(
    SnapshotId=latest_snapshot_id,
    AvailabilityZone="ap-southeast-2c",
    TagSpecifications=[
        {
            'ResourceType': 'volume',
            'Tags': [
                {
                    'Key': 'Name',
                    'Value': 'prod'
                },
            ]
        },
    ]
)

ec2 = boto3.resource('ec2')

# 4: attach/replace new volume to the given instance
while True:
    vol = ec2.Volume(new_volume['VolumeId'])
    print(vol.state)
    if vol.state == 'available':
        ec2.Instance(instance_id).attach_volume(
            Device='/dev/xvdb',
            VolumeId=new_volume['VolumeId']
        )
        break


