import schedule
import boto3
from operator import itemgetter

client = boto3.client('ec2', region_name="ap-southeast-2")

volumes = client.describe_volumes(
    Filters=[
        {
            'Name': 'tag:Name',
            'Values': ['prod']
        }
    ]
)['Volumes']

for volume in volumes:
    snapshots = client.describe_snapshots(
        OwnerIds=['self'],
        Filters=[
            {
                'Name': 'volume-id',
                'Values': [volume['VolumeId']]
            }
        ]
    )['Snapshots']

    sorted_by_date = sorted(snapshots, key=itemgetter('StartTime'), reverse=True)

    for snapshot in sorted_by_date[1:]:
        start_time = snapshot['StartTime']
        snapshot_id = snapshot['SnapshotId']
        print(start_time)
        response = client.delete_snapshot(
            SnapshotId=snapshot_id
        )

    print("\n**************************************************\n")
